package sao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.bea.common.security.xacml.IOException;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

import to.PropertiesTO;

@Path("/")
public class Operaciones {

	@GET
	@Path("parametro/{param}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<PropertiesTO> obtenerProperties(@PathParam("param") String propertie) {
		System.out.println("[obtenerProperties]propertie [" + propertie + "]");

		List<PropertiesTO> nodoLista = new ArrayList<PropertiesTO>();
		try {
			if (propertie != null && !propertie.equals("")) {
				
				String fichero = "";
				if (propertie.equalsIgnoreCase("header")) {
					fichero = "C:/Users/sentrauser3233/workspace/sao/tablas/header.properties";

				}
				if (propertie.equalsIgnoreCase("error")) {
					fichero = "C:/Users/sentrauser3233/workspace/sao/tablas/error.properties";
				}
				FileReader fr = new FileReader(fichero);
				BufferedReader br = new BufferedReader(fr);

				String linea;
				String namespace ="";
				
				while ((linea = br.readLine()) != null){
					String[] texto = linea.split(";");
					PropertiesTO dato = new PropertiesTO();
					namespace = texto[1];
					dato.setCodigo(namespace);
					nodoLista.add(dato);
				}
				fr.close();
				

			} else {
				System.out.println("[obtenerProperties]Error en datos de parametros");
				//return "Error en datos de parametros";
			}
		} 
		catch (Exception e) {
			System.out.println("[obtenerProperties]Error " + e);
			//return "Error en la operacion.";
		}
		return nodoLista;
	}
	
	@POST
	@Path("/xsd")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public PropertiesTO uploadFile(@FormDataParam("file") InputStream uploadedInputStream,
	        @FormDataParam("file") FormDataContentDisposition fileDetail) {
		 	
		        String uploadedFileLocation = "c://uploadedFiles/" + fileDetail.getFileName();
		        System.out.println("uploadedFileLocation:: "+uploadedFileLocation);
		 
		        // save it
		        saveToFile(uploadedInputStream, uploadedFileLocation);
		 
		        String output = "File uploaded via Jersey based RESTFul Webservice to: " + uploadedFileLocation;
		 
		        System.out.println(Response.status(200).entity(output).build());
		        PropertiesTO salida = new PropertiesTO();
		        salida.setCodigo(uploadedFileLocation);
		        return salida;
	}
	
	private void saveToFile(InputStream uploadedInputStream,
	        String uploadedFileLocation) {
	 
	        try {
	            OutputStream out = null;
	            int read = 0;
	            byte[] bytes = new byte[1024];
	 
	            out = new FileOutputStream(new File(uploadedFileLocation));
	            while ((read = uploadedInputStream.read(bytes)) != -1) {
	                out.write(bytes, 0, read);
	            }
	            out.flush();
	            out.close();
	        } 
	        catch (Exception e) {
	 
	            e.printStackTrace();
	        }
	 
	    }

}
